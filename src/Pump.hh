/*
 * Pump abstraction.
 *
 * Uses ESP32 LEDC PWM mechanism.
 */

#pragma once

#include "driver/ledc.h"

class Pump
{
private:
    const int              theGpioPin;
    const ledc_channel_t   theLedcChannel;
    const ledc_timer_t     theLedcTimer;
    const ledc_mode_t      theLedcSpeedMode;
    const ledc_timer_bit_t theLedcResolution;
    const uint32_t         theLedcFrequency;

public:
    // Note that the higher the frequency, the less resolution is available
    // see https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/peripherals/ledc.html#supported-range-of-frequency-and-duty-resolutions

    Pump(   const int              gpioPin,   // e.g. 2 for onboard blue LED
            const ledc_channel_t   ledcChannel    = LEDC_CHANNEL_0,
            const ledc_timer_t     ledcTimer      = LEDC_TIMER_0,
            const ledc_mode_t      ledcSpeedMode  = LEDC_HIGH_SPEED_MODE,
            const ledc_timer_bit_t ledcResolution = LEDC_TIMER_10_BIT,
            const uint32_t         ledcFrequency  = 20000 )
        :
            theGpioPin(        gpioPin        ),
            theLedcChannel(    ledcChannel    ),
            theLedcTimer(      ledcTimer      ),
            theLedcSpeedMode(  ledcSpeedMode  ),
            theLedcResolution( ledcResolution ),
            theLedcFrequency(  ledcFrequency  )
    {}

    void init();

    void set(
            int newValue );
};

