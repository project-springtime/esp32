/**
 * The task that runs the console command processor and everything related
 */

#include "argtable3/argtable3.h"
#include "linenoise/linenoise.h"
#include "esp_console.h"
#include "command_task.hh"
#include "cmd_system.h"
#include "esp_vfs_dev.h"
#include "driver/uart.h"

#include "Controller.hh"

#include <unistd.h>


/*
#include "esp_system.h"
#include "esp_log.h"
#include <string.h>
#include "esp_vfs_fat.h"
#include "nvs.h"
#include "nvs_flash.h"

#include "springtime.hh"
*/

static const uart_port_t UART_NUMBER_0 = UART_NUM_0;

static struct {
    struct arg_str *key;
    struct arg_end *end;
} pget_args;

static struct {
    struct arg_str *key;
    struct arg_str *value;
    struct arg_end *end;
} pset_args;

static struct {
    struct arg_end *end;
} plist_args;

static struct {
    struct arg_end *end;
} psave_args;

static struct {
    struct arg_end *end;
} pdeleteall_args;

static struct {
    struct arg_end *end;
} time_args;

static struct {
    struct arg_str *value;
    struct arg_end *end;
} settime_args;


static int cmd_pget(
    int     argc,
    char ** argv )
{
    int nerrors = arg_parse(argc, argv, (void **) &pget_args);
    if (nerrors != 0) {
        arg_print_errors(stderr, pget_args.end, argv[0]);
        return 1;
    }

    const char *key = pget_args.key->sval[0];

    int value = CONTROLLER.getParametersP()->get( key );

    printf( "%d\n", value );

    return 0;
}

static int cmd_pset(
    int     argc,
    char ** argv )
{
    int nerrors = arg_parse(argc, argv, (void **) &pset_args);
    if (nerrors != 0) {
        arg_print_errors(stderr, pset_args.end, argv[0]);
        return 1;
    }

    const char *key   = pset_args.key->sval[0];
    const char *value = pset_args.value->sval[0];

    CONTROLLER.getParametersP()->set( key, strtol( value, NULL, 0 ));

    return 0;
}

static int cmd_plist(
    int     argc,
    char ** argv )
{
    int nerrors = arg_parse(argc, argv, (void **) &plist_args);
    if (nerrors != 0) {
        arg_print_errors(stderr, plist_args.end, argv[0]);
        return 1;
    }
    CONTROLLER.getParametersP()->printList();

    return 0;
}

static int cmd_psave(
    int     argc,
    char ** argv )
{
    int nerrors = arg_parse(argc, argv, (void **) &psave_args);
    if (nerrors != 0) {
        arg_print_errors(stderr, psave_args.end, argv[0]);
        return 1;
    }

    CONTROLLER.getParametersP()->saveToNvs();

    return 0;
}

static int cmd_pdeleteall(
    int     argc,
    char ** argv )
{
    int nerrors = arg_parse(argc, argv, (void **) &pdeleteall_args);
    if (nerrors != 0) {
        arg_print_errors(stderr, pdeleteall_args.end, argv[0]);
        return 1;
    }

    CONTROLLER.getParametersP()->deleteAllFromNvs();

    return 0;
}

static int cmd_time(
    int     argc,
    char ** argv )
{
    printf("FIXME: cmd_time\n");
    return 0;
}

static int cmd_settime(
    int     argc,
    char ** argv )
{
    printf("FIXME: cmd_settime\n");
    return 0;
}

static void register_commands(void) {

    CONTROLLER.getParametersP()->loadFromNvs();

    pget_args.key = arg_str1(NULL, NULL, "<name>", "name of the parameter to be read");
    pget_args.end = arg_end(2);

    pset_args.key   = arg_str1(NULL, NULL, "<name>", "name of the parameter to be written");
    pset_args.value = arg_str1(NULL, NULL, "<value>", "value of the parameter to be written");
    pset_args.end   = arg_end(2);

    plist_args.end = arg_end(2);

    psave_args.end = arg_end(2);

    pdeleteall_args.end = arg_end(2);

    time_args.end = arg_end(2);

    settime_args.value = arg_str1(NULL, NULL, "<time>", "value of current UNIX epoch");
    settime_args.end = arg_end(2);


    const esp_console_cmd_t cmds [] = {
        {
            .command  = "pget",
            .help     = "Read a parameter",
            .hint     = NULL,
            .func     = &cmd_pget,
            .argtable = &pget_args
        },
        {
            .command  = "pset",
            .help     = "Set a parameter",
            .hint     = NULL,
            .func     = &cmd_pset,
            .argtable = &pset_args
        },
        {
            .command  = "plist",
            .help     = "List parameters",
            .hint     = NULL,
            .func     = &cmd_plist,
            .argtable = &plist_args
        },
        {
            .command  = "psave",
            .help     = "Save parameters to flash",
            .hint     = NULL,
            .func     = &cmd_psave,
            .argtable = &psave_args
        },
        {
            .command  = "pdeleteall",
            .help     = "Delete all parameters in flash",
            .hint     = NULL,
            .func     = &cmd_pdeleteall,
            .argtable = &pdeleteall_args
        },
        {
            .command  = "time",
            .help     = "Get the current time",
            .hint     = NULL,
            .func     = &cmd_time,
            .argtable = &time_args
        },
        {
            .command  = "settime",
            .help     = "Set the current time",
            .hint     = NULL,
            .func     = &cmd_settime,
            .argtable = &settime_args
        }
    };

    for( int i=0 ; i<sizeof( cmds ) / sizeof( cmds[0] ) ; ++i ) {
        ESP_ERROR_CHECK(
            esp_console_cmd_register( cmds + i ));
    }
}

static void initialize_console(void)
{
    /* Drain stdout before reconfiguring it */
    fflush( stdout);
    fsync( fileno( stdout ));

    /* Disable buffering on stdin */
    setvbuf( stdin, NULL, _IONBF, 0 );

    /* Minicom, screen, idf_monitor send CR when ENTER key is pressed */
    esp_vfs_dev_uart_set_rx_line_endings( ESP_LINE_ENDINGS_CR );
    /* Move the caret to the beginning of the next line on '\n' */
    esp_vfs_dev_uart_set_tx_line_endings( ESP_LINE_ENDINGS_CRLF );

    /* Configure UART. Note that REF_TICK is used so that the baud rate remains
     * correct while APB frequency is changing in light sleep mode.
     */
    const uart_config_t uart_config = {
            .baud_rate           = CONFIG_ESP_CONSOLE_UART_BAUDRATE,
            .data_bits           = UART_DATA_8_BITS,
            .parity              = UART_PARITY_DISABLE,
            .stop_bits           = UART_STOP_BITS_1,
            .flow_ctrl           = UART_HW_FLOWCTRL_DISABLE,
            .rx_flow_ctrl_thresh = 0,
            .use_ref_tick        = 0
    };

    /* Install UART driver for interrupt-driven reads and writes */
    ESP_ERROR_CHECK(
        uart_driver_install( UART_NUMBER_0, 256, 0, 0, NULL, 0 ));

    ESP_ERROR_CHECK(
        uart_param_config( UART_NUMBER_0, &uart_config ));

    /* Tell VFS to use UART driver */
    esp_vfs_dev_uart_use_driver( UART_NUMBER_0 );

    /* Initialize the console */
    esp_console_config_t console_config = {
            .max_cmdline_length = 256,
            .max_cmdline_args   = 8,
            .hint_color         = 0,
            .hint_bold          = 0
    };
    ESP_ERROR_CHECK(
        esp_console_init( &console_config ));

    /* Configure linenoise line completion library */
    /* Enable multiline editing. If not set, long commands will scroll within
     * single line.
     */
    linenoiseSetMultiLine(1);

    /* Tell linenoise where to get command completions and hints */
    linenoiseSetCompletionCallback( &esp_console_get_completion );
    linenoiseSetHintsCallback( (linenoiseHintsCallback*) &esp_console_get_hint );

    /* Set command history size */
    linenoiseHistorySetMaxLen( 100 );
}

extern "C"
void command_task()
{
    initialize_console();
    /* Register commands */
    esp_console_register_help_command();
    register_system();
    register_commands();

    const char* prompt = "springtime> ";

    printf("\n"
           "Project Springtime ESP32 shell.\n"
           "Type 'help' to get the list of commands.\n"
           "Use UP/DOWN arrows to navigate through command history.\n"
           "Press TAB when typing command name to auto-complete.\n");

    /* Figure out if the terminal supports escape sequences */

    int probe_status = linenoiseProbe();
    if( probe_status ) { /* zero indicates success */
        printf( "\n"
                "Your terminal application does not support escape sequences.\n"
                "Line editing and history features are disabled.\n"
                "On Windows, try using Putty instead.\n" );
        linenoiseSetDumbMode(1);
    }

    while( true ) {
        char * line = linenoise( prompt );
        if( line == NULL ) {
            continue;
        }

        linenoiseHistoryAdd( line );

        int ret;
        esp_err_t err = esp_console_run( line, &ret );

        if( err == ESP_ERR_NOT_FOUND ) {
            printf( "Command not found\n" );

        } else if( err == ESP_ERR_INVALID_ARG ) {
            // command was empty

        } else if( err == ESP_OK && ret != ESP_OK ) {
            printf( "Command returned non-zero error code: 0x%x (%s)\n", ret, esp_err_to_name( ret ));

        } else if( err != ESP_OK ) {
            printf( "Internal error: %s\n", esp_err_to_name( err ));
        }

        linenoiseFree( line );
    }
}


