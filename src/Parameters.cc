#include "Parameters.hh"

#include <stdio.h>
#include <string.h>
#include "esp_system.h"
#include "esp_log.h"
#include "esp_console.h"
#include "esp_vfs_dev.h"
#include "driver/uart.h"
#include "linenoise/linenoise.h"
#include "argtable3/argtable3.h"
#include "cmd_system.h"
#include "esp_vfs_fat.h"
#include "nvs.h"
#include "nvs_flash.h"

/* non-volatile storage namespace name */
static char nvs_namespace[16] = "springtime";

Parameters::Parameters(
        const int         nPars,
        Parameter * const pars )
    :
        theNPars( nPars ),
        thePars( pars )
{}

int Parameters::get(
        const char * name ) const
{
    for( int i=0 ; i<theNPars ; ++i ) {
        if( strcmp( name, thePars[i].getName() ) == 0 ) {
            return thePars[i].get();
        }
    }
    return -1;
}

void Parameters::set(
        const char * name,
        const int    newValue )
{
    for( int i=0 ; i<theNPars ; ++i ) {
        if( strcmp( name, thePars[i].getName() ) == 0 ) {
            thePars[i].set( newValue );

        }
    }
}

void Parameters::printList()
{
    for( int i=0 ; i<theNPars ; ++i ) {
        printf( "%s\n", thePars[i].getName() );
    }
}

void Parameters::loadFromNvs()
{
    nvs_handle_t nvs;
    esp_err_t    err;

    err = nvs_open( nvs_namespace, NVS_READONLY, &nvs );
    if (err != ESP_OK) {
        printf( "ERROR opening nvs\n" );
        return;
    }

    for( int i=0 ; i<theNPars ; ++i ) {
        printf( "Attempting to read %s\n", thePars[i].getName() );

        int32_t value;
        if( (err = nvs_get_i32( nvs, thePars[i].getName(), &value)) == ESP_OK) {
            thePars[i].set( (int) value );
        }
    }
    printf( "Done attempting to read\n" );

    nvs_close(nvs);

    printf( "Done closing\n" );
}

void Parameters::saveToNvs()
{
    nvs_handle_t nvs;
    esp_err_t    err;

    err = nvs_open( nvs_namespace, NVS_READWRITE, &nvs );
    if (err != ESP_OK) {
        printf( "ERROR opening nvs\n" );
        return;
    }

    for( int i=0 ; i<theNPars ; ++i ) {
        err = nvs_set_i32( nvs, thePars[i].getName(), (int32_t) thePars[i].get() );
        if (err != ESP_OK) {
            printf( "ERROR setting nvs parameter %s\n", thePars[i].getName() );
        } else {
            printf( "SUCCESS setting nvs parameter %s\n", thePars[i].getName() );
        }
    }

    if (err == ESP_OK) {
        err = nvs_commit( nvs );
        if (err == ESP_OK) {
            printf( "Parameters written to NVS\n" );
        }
    }

    nvs_close(nvs);
}

void Parameters::deleteAllFromNvs()
{
    nvs_handle_t nvs;
    esp_err_t    err;

    err = nvs_open( nvs_namespace, NVS_READWRITE, &nvs );
    if (err == ESP_OK) {
        err = nvs_erase_all( nvs );
        if (err == ESP_OK) {
            err = nvs_commit( nvs );
        }
    }
    if (err != ESP_OK) {
        printf( "ERROR when attempting to deleteAllFromNvs\n" );
    }

    nvs_close(nvs);
}


